﻿//-----------------------------------------------------------------------
// <copyright file="PersistentSingletonMonoBehaviour.cs">
//  Copyright (c) 2015, Sam Bolton
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice,
//  this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
//  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
//-----------------------------------------------------------------------

namespace ReusableScripts.MonoBehaviours
{
    using UnityEngine;

    /// <summary>
    /// Provides singleton functionality for the class that will persist through scenes. 
    /// </summary>
    /// <typeparam name="T">
    /// The class that will be a persistent singleton. 
    /// </typeparam>
    public abstract class PersistentSingletonMonoBehaviour<T> : SingletonMonoBehaviour<T> where T : MonoBehaviour
    {
        #region Protected Methods

        /// <summary>
        /// Method that will be called during awake. 
        /// </summary>
        /// <remarks>
        /// Use this method instead of awake so that base calls from AdvancedMonoBehaviour can be
        /// called as well. This will still be called during the awake call. Call the base function
        /// of this class in order for the singleton to work properly.
        /// </remarks>
        protected override void AdvancedAwake()
        {
            // Call the base reference of this class 
            base.AdvancedAwake();

            // Make this a persistent object 
            DontDestroyOnLoad(gameObject);
        }

        #endregion Protected Methods
    }
}