﻿//-----------------------------------------------------------------------
// <copyright file="PooledObject.cs">
//  Copyright (c) 2015, Sam Bolton
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice,
//  this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
//  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
//-----------------------------------------------------------------------

namespace ReusableScripts.ObjectPooler
{
    using MonoBehaviours;
    using UnityEngine;

    /// <summary>
    /// Placed on an object that will part of an object pool.
    /// </summary>
    /// <typeparam name="T">The class that is the object pooler.</typeparam>
    public abstract class PooledObject<T> : AdvancedMonoBehaviour where T : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// Reference to the pooler instance that this works with.
        /// </summary>
        private ObjectPool<T> _pooler;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// Sets the reference to the pooler instance that this works with.
        /// </summary>
        public ObjectPool<T> Pooler
        {
            set
            {
                if (_pooler == null)
                    _pooler = value;
            }
        }

        #endregion Public Properties

        #region Protected Methods

        /// <summary>
        /// Method that will run in order to deactivate the object.
        /// </summary>
        protected void DeactivateObject()
        {
            PreDeactivation();

            transform.SetParent(_pooler.transform);
            gameObject.SetActive(false);

            PostDeactivation();
        }

        /// <summary>
        /// Method that will run after the object is deactivated.
        /// </summary>
        protected abstract void PostDeactivation();

        /// <summary>
        /// Method that will run before the object is deactivated.
        /// </summary>
        protected abstract void PreDeactivation();

        #endregion Protected Methods
    }
}